<?php
function hashPassword( string $password ): string
{
    // on passe le hash avec un algo sha256 sur les trois éléments à encoder
    $salt_hash = hash( 'sha256', SALT );
    $pepper_hash = hash( 'sha256', PEPPER );
    $password_hash = hash( 'sha256', $password );

    // on concatène les trois hash obtenus
    $hash_string = $salt_hash . $password_hash . $pepper_hash;

    // on passe a nouveau le hash sur le résutat précédent
    $result  = hash( 'sha256', $hash_string );

    return $result;
}


function cate_list( mysqli_result $result ): string
{
    // Construction du HTML de la liste des catéories
    $html = '<ul class="cate-ul">';

    // Lecture du tableau des résultats
    while( $row = mysqli_fetch_assoc( $result ) ) {
        $html .= '<li class="cate-li"> <a class="cate-a" href="http://tp-php-mysql.test/?url=sujets&cat=' . $row[ 'id' ] . '">' . $row[ 'name' ] . '</a> </li>';
    }

    $html .= '</ul>';

    return $html;
}

