<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Connexion</title>

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <h1>Connexion</h1>

    <form method="post">
        <input type="text" name="username" placeholder="Identifiant"><br>
        <input type="password" name="password" placeholder="Mot de passe">

        <button type="submit">Connexion</button>
    </form>
</body>
</html>