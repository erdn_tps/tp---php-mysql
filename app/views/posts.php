<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Titre sujet</title>

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <h1>PAGE POSTS</h1><br>
    <a class="return-link" href="http://tp-php-mysql.test/">retour aux catégories</a>

<div class="main-aside-wrapper">
    <main class="list">
        <?php echo $html_subject; ?>
    </main>

    <aside class="form-posts">
        <form method="POST">
            <label>
                <input class="input-text" type="text" name="post_name" placeholder="Nom du sujet">
            </label>
            <br>
            <label>
                <div class="white-space">
                    <textarea class="input-textarea" name="post_content" id="" cols="30" rows="10" placeholder="Contenu du sujet"></textarea>
                </div>
            </label>

            <input class="input-btn" type="submit" value="Créer">
        </form>
    </aside>
</div>
    
</body>
</html>