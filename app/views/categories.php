<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MonSuperForum</title>

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <header>
        <h1>MonSuperForum</h1>
    </header>

<div class="main-aside-wrapper">
    <main class="list">
        <?php echo $cate_list; ?>
    </main>

    <aside class="form">
        <form method="POST">
            <label>
                <input class="input-text" type="text" name="category_name" placeholder="Nom de la catégorie">
            </label><br>

            <input class="input-btn" type="submit" value="Créer">
        </form>
    </aside>
</div>

</body>
</html>