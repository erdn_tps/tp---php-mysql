<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Titre catégorie</title>

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <header>
        <h1>PAGE SUJETS</h1><br>
        <a class="return-link" href="http://tp-php-mysql.test/">retour aux catégories</a>
    </header>

<div class="main-aside-wrapper">
    <main class="list">
        <?php echo $html_cat; ?>
    </main>

    <aside class="form-subject">
        <form method="POST">
            <label>
                <input class="input-text" type="text" name="subject_name" placeholder="Nom du sujet">
            </label><br>

            <input class="input-btn" type="submit" value="Créer">
        </form>
    </aside>
</div>
    
</body>
</html>