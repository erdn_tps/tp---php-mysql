<?php
require_once 'app/utils.php';

$cat_id = !empty( $_GET[ 'cat' ] ) ? $_GET[ 'cat' ] : '';

$html_cat = ''; 

if($_SERVER[ 'REQUEST_METHOD' ] === 'POST' && !empty($cat_id) ) {

    $name = !empty( $_POST[ 'subject_name' ] ) ? $_POST[ 'subject_name' ] : '';

    $sql = 'INSERT INTO `subjects` SET title=? , category_id=?';
    
    if ( $stmt = mysqli_prepare( $mysql, $sql ) ) {
    
        mysqli_stmt_bind_param( $stmt, 'si', $name, $cat_id );
        mysqli_stmt_execute( $stmt );

        mysqli_stmt_close($stmt);
    }
}


    $sel_id = 'SELECT * FROM subjects WHERE category_id=? ';
    
    if ( $stmt = mysqli_prepare( $mysql, $sel_id ) ) {
    
        mysqli_stmt_bind_param( $stmt, 'i', $cat_id );
        mysqli_stmt_execute( $stmt );

        $result = mysqli_stmt_get_result( $stmt );

        $html_cat = '<ul class="subject-ul">';
        
        while( $row = mysqli_fetch_assoc( $result ) ) {
            $timestamp = strtotime( $row['date_creation'] );
            $date_display = strftime( '%A %e %B %Y à %X', $timestamp);
            
            $html_cat .= '<li class="subject-li"> <a class="subject-a" href="http://tp-php-mysql.test/?url=posts&subject_id=' . $row[ 'id' ] . '">' . $row['title'] . ' <br> <span class="subject-date">Posted on: ' . $date_display . '</span></li>' ;
        }
        
        $html_cat .= '</ul>';
    }