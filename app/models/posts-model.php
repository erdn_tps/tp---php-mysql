<?php

$subject_id = !empty( $_GET[ 'subject_id' ] ) ? $_GET[ 'subject_id' ] : '';

$html_subject = ''; 

if($_SERVER[ 'REQUEST_METHOD' ] === 'POST' && !empty($subject_id) ) {

    $name = !empty( $_POST[ 'post_name' ] ) ? $_POST[ 'post_name' ] : '';
    $content = !empty( $_POST[ 'post_content' ] ) ? $_POST[ 'post_content' ] : '';

    $sql = 'INSERT INTO `posts` SET title=? , content=?, subject_id=?';
    
    if ( $stmt = mysqli_prepare( $mysql, $sql ) ) {
    
        mysqli_stmt_bind_param( $stmt, 'ssi', $name, $content, $subject_id );
        mysqli_stmt_execute( $stmt );

        mysqli_stmt_close($stmt);
    }
}


    $sel_id = 'SELECT * FROM posts WHERE subject_id=? ';
    
    if ( $stmt = mysqli_prepare( $mysql, $sel_id ) ) {
    
        mysqli_stmt_bind_param( $stmt, 'i', $subject_id );
        mysqli_stmt_execute( $stmt );

        $result = mysqli_stmt_get_result( $stmt );

        $html_subject = '<ul class="posts-ul">';

        while( $row = mysqli_fetch_assoc( $result ) ) {
            $timestamp = strtotime( $row['date_creation'] );
            $date_display = strftime( '%A %e %B %Y à %X', $timestamp);

            $html_subject .= '<li class="posts-li"> <span class="posts-title-date">' . $row['title'] . ' <br> <span class="posts-date">Posted on: ' . $date_display . '</span></span><div class="posts-content">' . $row['content'] .  '</div></li>' ;
        }

        $html_subject .= '</ul>';
    }