<?php
require_once 'app/utils.php';

global $mysql;

// ** REQUETE POUR AFFICHER LES CATEGORIES
$cate_list = '';

$categories = 'SELECT id, `name`
            FROM categories';

$stmt = mysqli_prepare( $mysql, $categories );

    mysqli_stmt_execute( $stmt );

    $result = mysqli_stmt_get_result( $stmt );

    $cate_list = cate_list( $result );


// ** REQUETE POUR AJOUTER UNE CATEGORIE
if($_SERVER[ 'REQUEST_METHOD' ] === 'POST' ){

    $name = !empty( $_POST[ 'category_name' ] ) ? $_POST[ 'category_name' ] : '';

    $sql = 'INSERT INTO categories 
            SET `name`=? ';
            
    if ( $stmt = mysqli_prepare( $mysql, $sql ) ) {
    
        mysqli_stmt_bind_param( $stmt, 's', $name );
        mysqli_stmt_execute( $stmt );
        
    }
}
