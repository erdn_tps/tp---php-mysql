<?php
require_once 'app/utils.php';

function getLogin( string $username, $password ): ?array
{
    global $mysql;

    $result = [];

    $req = 'SELECT id, username
            FROM users
            WHERE 
                username = ?
                AND `password` = ?';

    if( $stmt = mysqli_prepare( $mysql, $req ) ) {

        $password_hash = hashPassword( $password );

        mysqli_stmt_bind_param( $stmt, 'ss', $username, $password_hash );
        mysqli_stmt_execute( $stmt );

        $req_result = mysqli_stmt_get_result( $stmt );

        $result = mysqli_fetch_assoc( $req_result );
    }

    return $result;
} 
