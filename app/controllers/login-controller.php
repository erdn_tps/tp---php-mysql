<?php
require_once PATH_MODELS . 'login-model.php';

// si on a posté le formulaire
if( $_SERVER[ 'REQUEST_METHOD' ] === 'POST' ) {
    
    // on récupère les champs saisis
    $username = !empty( $_POST[ 'username' ] ) ? $_POST[ 'username' ] : '';
    $password = !empty( $_POST[ 'password' ] ) ? $_POST[ 'password' ] : '';
    
    // tous les champs son saisis ? => check en BDD
    if( !empty( $username ) && !empty( $password ) ) {

        // on appelle le modèle pour trouver l'utilisateur
        $user = getLogin( $username, $password );

        // le modèle renvoi-t-il un utilisateur ? => enregistrement dans la session
        if( $user !== null ) {
            $_SESSION[ 'user' ] = $user;

            header( 'Location: http://tp-php-mysql.test/?url=categories' );

            die();
        }
    }
    
}

require_once PATH_VIEWS . 'login.php';