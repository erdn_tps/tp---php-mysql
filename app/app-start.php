<?php
// constantes de chemins
define( 'PATH_CONTROLLERS', 'app/controllers/' );
define( 'PATH_VIEWS', 'app/views/' );
define( 'PATH_MODELS', 'app/models/' );

// demarrage session
session_start();

// connection de la BDD
$mysql = mysqli_connect( DB_HOST, DB_USER, DB_PASS, DB_NAME );

// si l'url n'est pas vide, aller sur l'url, sinon redirection vers les categories
$page = !empty( $_GET[ 'url' ] ) ? $_GET[ 'url' ] : 'categories';

switch( $page ) {
    case 'login':
    require_once PATH_CONTROLLERS . 'login-controller.php';
        break;

    case 'categories' :
    require_once PATH_CONTROLLERS . 'categories-controller.php';
        break;

    case 'sujets' :
    require_once PATH_CONTROLLERS . 'subjects-controller.php';
        break;

    case 'posts' :
    require_once PATH_CONTROLLERS . 'posts-controller.php';
        break;

    default:
        break;
}

// ----- Traitement des dates -----

    // On peut redéfinir le fuseau horaire à utiliser (si on ne souhaite pas modifier la configuration du serveur)
    date_default_timezone_set( 'Europe/Paris' );

    // On peut aussi redéfinir la langue et la culture à utiliser (pour le nom des jours et des mois)
    setlocale( LC_ALL, 'fr_FR' );